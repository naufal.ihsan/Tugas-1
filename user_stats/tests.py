from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from assets.config import NAME
from update_status.models import Status
from add_friends.models import AddFriends
# Create your tests here.


class UserStatsUnitTest(TestCase):

    def test_user_stats_is_exist(self):
        response = Client().get('/user-stats/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/user-stats/')
        self.assertEqual(found.func, index)