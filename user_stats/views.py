from django.shortcuts import render
from add_friends.models import AddFriends
from update_status.models import Status
from assets.config import NAME, IMAGE

# Create your views here.

response = {}

def index(request):
    html = 'user_stats.html'
    response['author'] = NAME
    response['image'] = IMAGE
    friends = AddFriends.objects.count()
    response['friends'] = friends
    feeds = Status.objects.count()
    response['feeds'] = feeds
    if(feeds!=0):
        latest_post = Status.objects.first()
        response['latest_post'] = latest_post.status
        response['latest_post_date'] = latest_post.created_at
    return render(request,html,response)