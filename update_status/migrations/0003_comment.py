# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-12 17:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('update_status', '0002_auto_20171008_2042'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('status', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='update_status.Status')),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
    ]
