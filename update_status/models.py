from django.db import models


class Status(models.Model):

    status = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Statuses"
        ordering = ['-created_at']


class Comment(models.Model):

    comment = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    status = models.ForeignKey(
        Status,
        on_delete=models.CASCADE,
        related_name="comments")

    class Meta:
        ordering = ['created_at']
