from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from update_status.views import index
from update_status.models import Status


class UpdateStatusUnitTest(TestCase):
    def test_model_created(self):
        Status.objects.create(status="Hello")
        Status.objects.create(status="I'm very handsome")
        self.assertNotEqual(Status.objects.get(status="Hello"), None)
        self.assertNotEqual(Status.objects.get(status="I'm very handsome"), None)
        Status.objects.all().delete()

    def test_landing_page_is_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_post_working(self):
        msg = 'hitler did nothing wrong'
        response = Client().post('/', {"status": msg})
        self.assertEqual(response.status_code, 200)
        self.assertIn(msg, response.content.decode("utf8"))

    def test_is_static_css_loaded(self):
        response = Client().get('/static/css/update_status.css')
        self.assertEqual(response.status_code, 200)

    def test_add_comment(self):
        last = Status.objects.create(status="lol")
        msg = 'ini komentar'
        print('/%d/' % last.id)
        response = Client().post('/%d/' % last.id, {"comment": msg})
        self.assertEqual(response.status_code, 302)
        response = Client().get('/')
        self.assertIn(msg, response.content.decode("utf8"))
