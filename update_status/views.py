from django.http import HttpResponseRedirect
from django.shortcuts import render
from update_status.models import Status
from update_status.forms import StatusForm, CommentForm
from assets.config import NAME, IMAGE


def index(request):
    response = {}
    html = 'update_status.html'

    response = {
        'statuses': Status.objects.all(),
        'form': StatusForm(),
        'user': {
            'name': NAME,
            'imgurl': IMAGE
        },
    }

    if request.method == 'POST':
        response['form'] = StatusForm(request.POST)
        if response['form'].is_valid():
            response['form'].save()

    return render(request, html, response)


def add_comment(request, status_id):
    if request.method == "POST":
        form = CommentForm({
            'status': status_id,
            'comment': request.POST['comment']
        })
        if form.is_valid():
            form.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
