from django.conf.urls import url
from .views import index, add_comment

urlpatterns = [
    url(r'^$' , index, name='index'),
    url(r'^(?P<status_id>\d+)/$', add_comment, name='add_comment')
]