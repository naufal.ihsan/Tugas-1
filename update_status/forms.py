from django import forms
from update_status.models import Status, Comment


class StatusForm(forms.ModelForm):
    status = forms.CharField(
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'rows': 3,
            'id': 'id_status',
            'placeholder': 'What\'s happening?'
        })
    )

    class Meta:
        model = Status
        fields = ['status']


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['comment', 'status']
