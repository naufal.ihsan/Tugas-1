"""Tugas_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
import add_friends.urls as add_friends
import update_status.urls as update_status
import user_profile.urls as user_profile
import user_stats.urls as user_stats

urlpatterns = [
    url(r'', include(update_status,namespace='update-status')),
    url(r'^admin/', admin.site.urls),
    url(r'^add-friends/', include(add_friends,namespace='add-friends')),
    url(r'^user-profile/', include(user_profile,namespace='user-profile')),
    url(r'^user-stats/', include(user_stats,namespace='user-stats')),
]
