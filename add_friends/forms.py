from django import forms
from add_friends.models import AddFriends

class AddFriendsForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'add-friends-input-name',
            'placeholder':' Add by username',
        })
    )

    url = forms.URLField(
        widget=forms.URLInput(attrs={
             'class': 'add-friends-input-url',
             'placeholder':' Insert url here',
        })
    )

    class Meta:
        model = AddFriends
        fields = ['name','url']        

        

			
			