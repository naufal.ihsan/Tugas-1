# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-07 08:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('add_friends', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='add_friends',
            name='added_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
