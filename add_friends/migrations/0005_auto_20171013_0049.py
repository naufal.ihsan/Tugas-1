# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-12 17:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('add_friends', '0004_auto_20171011_1510'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addfriends',
            name='url',
            field=models.URLField(),
        ),
    ]
