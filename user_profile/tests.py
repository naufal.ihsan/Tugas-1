from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index,expertise
# Create your tests here.


class UserProfileUnitTest(TestCase):

    def test_user_profile_is_exist(self):
        response = Client().get('/user-profile/')
        self.assertEqual(response.status_code, 200)

    def test_user_profile_using_index_func(self):
        found = resolve('/user-profile/')
        self.assertEqual(found.func, index)

    def test_user_profile_url_is_exist(self):
        response = Client().get('/user-profile/edit-profile')
        self.assertEqual(response.status_code, 200)

    def test_user_expertise_shown_in_page(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        # Checking all family member is shown in page
        for expert in expertise :
            self.assertIn('<span class="label label-info">'+ expert +'</span>', html_response)
