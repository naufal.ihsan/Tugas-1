from django.shortcuts import render
from assets.config import NAME, IMAGE
# Create your views here.
expertise = ['Bald','Enemies with Squidward','Lives on 150 Shell Street']
response = {}
def index(request):
    response['author'] = NAME
    response['profile_picture'] = IMAGE
    response['birthday'] = "14 July"
    response['gender'] = "Male"
    response['expertise'] = expertise
    response['description'] = "Last customer who SpongeBob and Squidward were required to learn the name of in the episode Good Ol Whatshisname to win a prize."
    response['email'] = "mrwhatzit@tooya.com"
    html = 'user_profile.html'
    return render(request,html,response)

def edit(request):
    response['profile_picture'] = IMAGE
    html = 'edit_profile.html'
    return render(request,html,response)